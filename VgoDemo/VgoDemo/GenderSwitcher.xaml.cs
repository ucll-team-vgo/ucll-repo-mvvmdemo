﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VgoDemo
{
    /// <summary>
    /// Interaction logic for GenderSwitcher.xaml
    /// </summary>
    public partial class GenderSwitcher : UserControl
    {
        public GenderSwitcher()
        {
            InitializeComponent();
        }
        
        public bool IsMale
        {
            get { return (bool) GetValue( IsMaleProperty ); }
            set { SetValue( IsMaleProperty, value ); }
        }

        public static readonly DependencyProperty IsMaleProperty =
            DependencyProperty.Register( "IsMale", typeof( bool ), typeof( GenderSwitcher ), new PropertyMetadata( true ) );
    }
}
