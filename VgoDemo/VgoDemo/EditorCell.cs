﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VgoDemo
{
    public class EditorCell<T> : Cell<T>, INotifyDataErrorInfo
    {
        private readonly ICell<T> cell;
        private readonly ICell<bool> isEdited;
        private readonly Func<T, bool> validator;
        private readonly ICell<bool> isValid;
        private readonly ICell<EditorStatus> status;

        public EditorCell( ICell<T> cell )
            : this( cell, x => true )
        {
            // NOP
        }

        public EditorCell( ICell<T> cell, Func<T, bool> validator )
            : base( cell.Value )
        {
            this.cell = cell;
            this.validator = validator;

            isEdited = Derived.Create( cell, this, ( x, y ) => !x.Equals( y ) );
            isValid = Derived.Create( this, validator );
            status = Derived.Create( isEdited, isValid, DeriveStatus );

            isValid.PropertyChanged += delegate( object sender, PropertyChangedEventArgs args )
            {
                if ( this.ErrorsChanged != null )
                {
                    this.ErrorsChanged( this, new DataErrorsChangedEventArgs( "Value" ) );
                }
            };
        }

        private static EditorStatus DeriveStatus( bool isEdited, bool isValid )
        {
            if ( !isEdited )
            {
                return EditorStatus.Unchanged;
            }
            else if ( isValid )
            {
                return EditorStatus.ValidChange;
            }
            else
            {
                return EditorStatus.InvalidChange;
            }
        }

        public ICell<bool> IsEdited
        {
            get
            {
                return isEdited;
            }
        }

        public ICell<EditorStatus> Status
        {
            get
            {
                return status;
            }
        }

        public void Commit()
        {
            cell.Value = this.Value;
        }

        public void Reset()
        {
            this.Value = cell.Value;
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public System.Collections.IEnumerable GetErrors( string propertyName )
        {
            if ( HasErrors )
            {
                yield return "Invalid value";
            }
        }

        public bool HasErrors
        {
            get
            {
                return !isValid.Value;
            }
        }
    }

    public enum EditorStatus
    {
        Unchanged,
        ValidChange,
        InvalidChange
    }
}
