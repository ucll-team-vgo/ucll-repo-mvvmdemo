﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VgoDemo
{
    public class GradeDatabase
    {
        public static GradeDatabase Example
        {
            get
            {
                return new GradeDatabase( ExampleStudents );
            }
        }

        private static IEnumerable<Student> ExampleStudents
        {
            get
            {
                yield return new Student( "Frederic", "Vogels", true, @"http://alexander.khleuven.be/VGO/topics/mvvm/brad.jpg" );
                yield return new Student( "Pieter", "Philippaerts", true, @"http://alexander.khleuven.be/VGO/topics/mvvm/pieter.jpg" );
                yield return new Student( "Keira", "K.", false, @"http://alexander.khleuven.be/VGO/topics/mvvm/keira.jpg" );
            }
        }

        public GradeDatabase( IEnumerable<Student> students )
        {
            Students = new ObservableCollection<Student>( students );
        }

        public ObservableCollection<Student> Students { get; set; }
    }

    public class Student
    {
        public Student( string fname, string lname, bool isMale, string picture )
        {
            FirstName = new Cell<string>( fname );
            LastName = new Cell<string>( lname );
            IsMale = new Cell<bool>( isMale );
            PictureUrl = new Cell<string>( picture );
        }

        public ICell<string> FirstName { get; private set; }

        public ICell<string> LastName { get; private set; }

        public ICell<bool> IsMale { get; private set; }

        public ICell<string> PictureUrl { get; private set; }

        public override string ToString()
        {
            return string.Format( "{0} {1}", FirstName.Value, LastName.Value );
        }

        public static bool IsValidFirstName(string firstName)
        {
            return !string.IsNullOrWhiteSpace( firstName );
        }

        public static bool IsValidLastName( string lastName )
        {
            return !string.IsNullOrWhiteSpace( lastName );
        }

        public static bool IsValidPictureUrl( string pictureUrl )
        {
            return !string.IsNullOrWhiteSpace( pictureUrl );
        }
    }
}
