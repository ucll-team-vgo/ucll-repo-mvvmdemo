﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace VgoDemo
{
    public class IsEditedConverter : IValueConverter
    {
        public object Convert( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            var isEdited = (bool) value;

            if ( targetType == typeof( FontWeight ) )
            {
                return isEdited ? FontWeights.Bold : FontWeights.Normal;
            }
            else if ( targetType == typeof(Brush))
            {
                return isEdited ? Brushes.Red : Brushes.Black;
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        public object ConvertBack( object value, Type targetType, object parameter, System.Globalization.CultureInfo culture )
        {
            throw new NotImplementedException();
        }
    }
}
