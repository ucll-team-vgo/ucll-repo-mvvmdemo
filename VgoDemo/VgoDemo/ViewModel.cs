﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace VgoDemo
{
    public class GradeDatabaseViewModel
    {
        private readonly GradeDatabase gradeDatabase;

        private readonly ObservableCollection<StudentViewModel> studentViewModels;

        public GradeDatabaseViewModel( GradeDatabase gradeDatabase )
        {
            this.gradeDatabase = gradeDatabase;

            studentViewModels = new ObservableCollection<StudentViewModel>( from student in gradeDatabase.Students
                                                                            select new StudentViewModel( student ) );
        }

        public ObservableCollection<StudentViewModel> Students
        {
            get
            {
                return studentViewModels;
            }
        }
    }

    public class StudentViewModel
    {
        private readonly Student student;
        private readonly EditorCell<string> firstName;
        private readonly EditorCell<string> lastName;
        private readonly ICell<string> fullName;
        private readonly EditorCell<string> pictureUrl;
        private readonly EditorCell<bool> isMale;
        private readonly ICell<bool> isEdited;
        private readonly ICell<bool> isResettable;
        private readonly ICell<bool> isCommittable;
        private readonly ICommand commitCommand;
        private readonly ICommand resetCommand;

        public StudentViewModel( Student student )
        {
            this.student = student;

            firstName = new EditorCell<string>( student.FirstName, Student.IsValidFirstName );
            lastName = new EditorCell<string>( student.LastName, Student.IsValidLastName );
            fullName = Derived.Create( firstName, lastName, ( fn, ln ) => string.Format( "{0} {1}", fn, ln ) );
            pictureUrl = new EditorCell<string>( student.PictureUrl, Student.IsValidPictureUrl );
            isMale = new EditorCell<bool>( student.IsMale );
            isEdited = Derived.Create( new List<ICell<bool>> { firstName.IsEdited, lastName.IsEdited, pictureUrl.IsEdited, isMale.IsEdited }, xs => xs.Any( p => p ) );
            isResettable = Derived.Create( new List<ICell<EditorStatus>> { firstName.Status, lastName.Status, pictureUrl.Status, isMale.Status }, DeriveIsResettable );
            isCommittable = Derived.Create( new List<ICell<EditorStatus>> { firstName.Status, lastName.Status, pictureUrl.Status, isMale.Status }, DeriveIsCommittable );
            commitCommand = new CommitCommand( this );
            resetCommand = new ResetCommand( this );
        }

        private bool DeriveIsResettable( IEnumerable<EditorStatus> statuses )
        {
            return statuses.Any( status => status != EditorStatus.Unchanged );
        }

        private bool DeriveIsCommittable( IEnumerable<EditorStatus> statuses )
        {
            return statuses.Any( status => status != EditorStatus.Unchanged ) && statuses.All( status => status != EditorStatus.InvalidChange );
        }

        public Cell<string> FirstName { get { return firstName; } }

        public Cell<string> LastName { get { return lastName; } }

        public ICell<string> FullName { get { return fullName; } }

        public Cell<string> PictureUrl { get { return pictureUrl; } }

        public Cell<bool> IsMale { get { return isMale; } }

        public ICell<bool> IsEdited { get { return isEdited; } }

        public ICell<bool> IsResettable { get { return isResettable; } }

        public ICell<bool> IsCommittable { get { return isCommittable; } }

        public ICommand Commit
        {
            get { return commitCommand; }
        }

        public ICommand Reset
        {
            get { return resetCommand; }
        }

        public void PerformCommit()
        {
            firstName.Commit();
            lastName.Commit();
            pictureUrl.Commit();
            isMale.Commit();
        }

        public void PerformReset()
        {
            firstName.Reset();
            lastName.Reset();
            pictureUrl.Reset();
            isMale.Reset();
        }
    }

    public class CommitCommand : ICommand
    {
        private readonly StudentViewModel studentViewModel;

        public CommitCommand( StudentViewModel studentViewModel )
        {
            this.studentViewModel = studentViewModel;

            studentViewModel.IsCommittable.PropertyChanged += ( sender, args ) => CanExecuteChanged( this, new EventArgs() );
        }

        public bool CanExecute( object parameter )
        {
            return studentViewModel.IsCommittable.Value;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute( object parameter )
        {
            studentViewModel.PerformCommit();
        }
    }

    public class ResetCommand : ICommand
    {
        private readonly StudentViewModel studentViewModel;

        public ResetCommand( StudentViewModel studentViewModel )
        {
            this.studentViewModel = studentViewModel;

            studentViewModel.IsResettable.PropertyChanged += ( sender, args ) => CanExecuteChanged( this, new EventArgs() );
        }

        public bool CanExecute( object parameter )
        {
            return studentViewModel.IsResettable.Value;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute( object parameter )
        {
            studentViewModel.PerformReset();
        }
    }
}
